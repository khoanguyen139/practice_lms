<?php

    date_default_timezone_set('Asia/Saigon');

    require(dirname(__FILE__).'/protected/globals.php');
    if($_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='lms.local' || $_SERVER['HTTP_HOST']=='lms.org' || $_SERVER['HTTP_HOST']=='lms.dev')
    {
        $yii=dirname(__FILE__).'/protected/vendor/framework/yii.php';
        $config=dirname(__FILE__).'/protected/config/dev.php';
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG',true);
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
    }
    else
    {
        $yii=dirname(__FILE__).'/protected/vendor/framework/yiilite.php';
        $config=dirname(__FILE__).'/protected/config/production.php';
        defined('YII_DEBUG') or define('YII_DEBUG',true);
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

    }
    require_once($yii);
    Yii::createWebApplication($config)->run();

?>