/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : lms_db

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2014-10-17 10:23:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for email_queues
-- ----------------------------
DROP TABLE IF EXISTS `email_queues`;
CREATE TABLE `email_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `from_name` varchar(50) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `to_name` text NOT NULL,
  `to_email` text NOT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_time` int(10) NOT NULL,
  `send_time` int(10) NOT NULL,
  `sent_time` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for email_templates
-- ----------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeframe_id` int(11) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `from_name` varchar(50) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `to_name` text NOT NULL,
  `to_email` text NOT NULL,
  `reply_to` varchar(10) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `resend_if_fail` tinyint(1) NOT NULL DEFAULT '1',
  `delete_if_success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) NOT NULL,
  `user_status` int(1) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
