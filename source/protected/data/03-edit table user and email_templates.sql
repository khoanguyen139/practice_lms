/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : lms_db

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2014-10-14 17:40:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for email_templates
-- ----------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeframe_id` int(11) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `from_name` varchar(50) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `to_name` text NOT NULL,
  `to_email` text NOT NULL,
  `reply_to` varchar(10) DEFAULT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `resend_if_fail` tinyint(1) NOT NULL DEFAULT '1',
  `delete_if_success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of email_templates
-- ----------------------------
INSERT INTO `email_templates` VALUES ('1', '1', 'User sign up new account', '(auto)', '(auto)', '(auto)', '(auto)', '(auto)', null, null, 'Chalksy - Sign up', '<div style=\"width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;\">\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;\" cellpadding=\"0\" cellspacing=\"5\">\n<tbody>\n	<tr style=\"background:#DDDCE2\">\n		<td><img src=\"http://chalksy.com/images/spacer.gif\" alt=\"\" height=\"12\" width=\"1\"></td>\n	</tr>\n\n	<tr>\n		<td style=\"padding:14px;\"><h1 class=\"current\" style=\"width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;\">Sign up</h1>\n<a href=\"http://chalksy.com\"><img src=\"http://chalksy.com/images/logo.png\" id=\"logo\" alt=\"logo\" style=\"width: 120px; float: right; margin-left: 60px; cursor: default;\"></a></td>\n	</tr>\n\n	<tr>\n		<td style=\"padding:10px;\"><div style=\"background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;\">\n<p class=\"current\" style=\"padding:14px;color:#222222;line-height:18px;\"> Hello,<br>\n<br>\n Welcome to <a href=\"http://chalksy.com/\" target=\"_blank\" style=\"color:#7d003f;\">Chalksy</a>. The innovative new auction management system. <br>\n<br>\n Thank you for registering! Please click on the link below to verify your account: <br>\n<a class=\"current\" href=\"[ACTIVATE_URL]\">[ACTIVATE_URL]</a><br>\n<br>\n Sincerely,<br>\n Chalksy team </p>\n</div>\n</td>\n	</tr>\n</tbody>\n</table>\n\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;\" cellpadding=\"0\" cellspacing=\"5\" width=\"100%\">\n<tbody>\n	<tr>\n		<td><div style=\"margin-right:auto;margin-left:auto;width:600px;\">\n<p style=\"width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;\"><br>\n If you are have any questions, please feel free to contact us at <a style=\"color:#fff\" href=\"mailto:contact@chalksy.com\">contact@chalksy.com</a> This email is a post only email. Replies to this email are not monitored or answered. </p>\n<a href=\"http://chalksy.com\"><img src=\"http://chalksy.com/images/logo-bt.png\" alt=\"logo\" style=\"width: 100px; float: right; margin-left: 60px; cursor: default;\"></a></div>\n</td>\n	</tr>\n</tbody>\n</table>\n</div>', '1', '0');
INSERT INTO `email_templates` VALUES ('2', '2', 'Forgot password', '', '', '', '', null, null, null, 'Forgot Password', '<div style=\"width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;\">\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;\" cellpadding=\"0\" cellspacing=\"5\">\r\n<tbody>\r\n	<tr style=\"background:#DDDCE2\">\r\n		<td><img src=\"http://chalksy.com/images/spacer.gif\" alt=\"\" height=\"12\" width=\"1\"></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td style=\"padding:14px;\">\r\n			<h1 class=\"current\" style=\"width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:34px;font-weight:normal;margin-top:20px;padding-left:8px;\">\r\n				CERES LMS - Password Reset Request\r\n			</h1>\r\n		</td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td style=\"padding:10px;\">\r\n			<div style=\"background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;\">\r\n				In order to reset your password at CERES LMS, you need to click <a href=\"[URL_RECOVERY]\">here</a><br />				\r\n				\r\n				If you did not request this email, you may safely ignore it.\r\n			</div>\r\n		</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n</div>', '1', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) DEFAULT NULL,
  `subscription_history_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_password_expiry_time` int(11) DEFAULT NULL,
  `user_activekey` varchar(128) NOT NULL,
  `user_created_time` int(11) DEFAULT NULL,
  `user_login_time` int(11) DEFAULT NULL,
  `user_last_visit_time` int(11) DEFAULT NULL,
  `user_status` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive',
  `user_no_of_reminder` int(2) DEFAULT NULL,
  `user_forgot_password_key` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '0', 'admin', 'admin@ceresolutions.com', '32091411de9ceee58948b8712ae94dac4a338c11cfa28322833bc8a1c8ef13ee ', '1388779199', '228652cc55474dbde', '1387267274', null, '1389122887', 'Active', null, '');
INSERT INTO `user` VALUES ('2', '2', '0', 'nxthuy', 'nxthuy@ceresolutions.com', '32091411de9ceee58948b8712ae94dac4a338c11cfa28322833bc8a1c8ef13ee ', null, '1', null, null, null, 'Active', null, '');
INSERT INTO `user` VALUES ('4', null, '0', 'nhphat', 'nhphat@ceresolutions.com', 'e10adc3949ba59abbe56e057f20f883e', null, '9908543cfcc089fa5', null, null, '1413283008', 'Inactive', null, '');
