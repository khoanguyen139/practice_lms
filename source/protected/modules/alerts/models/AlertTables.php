<?php

/**
 * This is the model class for table "alert_tables".
 *
 * The followings are the available columns in table 'alert_tables':
 * @property integer $alert_table_id
 * @property string $alert_table_name
 *
 * The followings are the available model relations:
 * @property Alert[] $alerts
 * @property AlertTableFields[] $alertTableFields
 */
class AlertTables extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alert_tables';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alert_table_name', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('alert_table_id, alert_table_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'alerts' => array(self::HAS_MANY, 'Alert', 'alert_table'),
			'alertTableFields' => array(self::HAS_MANY, 'AlertTableFields', 'alert_table_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'alert_table_id' => 'Alert Table',
			'alert_table_name' => 'Alert Table Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('alert_table_id',$this->alert_table_id);
		$criteria->compare('alert_table_name',$this->alert_table_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlertTables the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
