<?php

/**
 * This is the model class for table "alert_schedule".
 *
 * The followings are the available columns in table 'alert_schedule':
 * @property integer $alert_schedule_id
 * @property string $alert_schedule_name
 * @property string $alert_schedule_command
 * @property string $alert_schedule_status
 *
 * The followings are the available model relations:
 * @property Alert[] $alerts
 */
class AlertSchedule extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alert_schedule';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alert_schedule_name', 'length', 'max'=>30),
			array('alert_schedule_command', 'length', 'max'=>50),
			array('alert_schedule_status', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('alert_schedule_id, alert_schedule_name, alert_schedule_command, alert_schedule_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'alerts' => array(self::HAS_MANY, 'Alert', 'alert_schedule_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'alert_schedule_id' => 'Alert Schedule',
			'alert_schedule_name' => 'Alert Schedule Name',
			'alert_schedule_command' => 'Alert Schedule Command',
			'alert_schedule_status' => 'Alert Schedule Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('alert_schedule_id',$this->alert_schedule_id);
		$criteria->compare('alert_schedule_name',$this->alert_schedule_name,true);
		$criteria->compare('alert_schedule_command',$this->alert_schedule_command,true);
		$criteria->compare('alert_schedule_status',$this->alert_schedule_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlertSchedule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
