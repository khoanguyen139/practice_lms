<?php
/* @var $this AlertController */
/* @var $model Alert */

$this->breadcrumbs=array(
	'Alerts'=>array('index'),
	$model->alert_id,
);

$this->menu=array(
	array('label'=>'List Alert', 'url'=>array('index')),
	array('label'=>'Create Alert', 'url'=>array('create')),
	array('label'=>'Update Alert', 'url'=>array('update', 'id'=>$model->alert_id)),
	array('label'=>'Delete Alert', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->alert_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Alert', 'url'=>array('admin')),
);
?>

<h1>View Alert #<?php echo $model->alert_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'alert_id',
		'alert_category_id',
		'alert_schedule_id',
		'alert_table',
		'alert_subject',
		'alert_body',
		'alert_type',
	),
)); ?>
