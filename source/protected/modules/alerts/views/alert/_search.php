<?php
/* @var $this AlertController */
/* @var $model Alert */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'alert_id'); ?>
		<?php echo $form->textField($model,'alert_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_category_id'); ?>
		<?php echo $form->textField($model,'alert_category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_schedule_id'); ?>
		<?php echo $form->textField($model,'alert_schedule_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_table'); ?>
		<?php echo $form->textField($model,'alert_table'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_subject'); ?>
		<?php echo $form->textField($model,'alert_subject',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_body'); ?>
		<?php echo $form->textArea($model,'alert_body',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alert_type'); ?>
		<?php echo $form->textField($model,'alert_type',array('size'=>12,'maxlength'=>12)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->