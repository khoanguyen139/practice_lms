<?php
/* @var $this AlertTablesController */
/* @var $model AlertTables */

$this->breadcrumbs=array(
	'Alert Tables'=>array('index'),
	$model->alert_table_id,
);

$this->menu=array(
	array('label'=>'List AlertTables', 'url'=>array('index')),
	array('label'=>'Create AlertTables', 'url'=>array('create')),
	array('label'=>'Update AlertTables', 'url'=>array('update', 'id'=>$model->alert_table_id)),
	array('label'=>'Delete AlertTables', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->alert_table_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AlertTables', 'url'=>array('admin')),
);
?>

<h1>View AlertTables #<?php echo $model->alert_table_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'alert_table_id',
		'alert_table_name',
	),
)); ?>
