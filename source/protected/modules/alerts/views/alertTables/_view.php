<?php
/* @var $this AlertTablesController */
/* @var $data AlertTables */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_table_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->alert_table_id), array('view', 'id'=>$data->alert_table_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_table_name')); ?>:</b>
	<?php echo CHtml::encode($data->alert_table_name); ?>
	<br />


</div>