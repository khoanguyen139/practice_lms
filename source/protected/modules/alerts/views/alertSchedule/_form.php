<?php
/* @var $this AlertScheduleController */
/* @var $model AlertSchedule */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'alert-schedule-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_schedule_name'); ?>
		<?php echo $form->textField($model,'alert_schedule_name',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'alert_schedule_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_schedule_command'); ?>
		<?php echo $form->textField($model,'alert_schedule_command',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alert_schedule_command'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_schedule_status'); ?>
		<?php echo $form->textField($model,'alert_schedule_status',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'alert_schedule_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->