<?php
/* @var $this AlertTableFieldsController */
/* @var $data AlertTableFields */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_table_fields_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->alert_table_fields_id), array('view', 'id'=>$data->alert_table_fields_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_table_id')); ?>:</b>
	<?php echo CHtml::encode($data->alert_table_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_table_fields_name')); ?>:</b>
	<?php echo CHtml::encode($data->alert_table_fields_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_field_frequency')); ?>:</b>
	<?php echo CHtml::encode($data->alert_field_frequency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_table_fields_condition')); ?>:</b>
	<?php echo CHtml::encode($data->alert_table_fields_condition); ?>
	<br />


</div>