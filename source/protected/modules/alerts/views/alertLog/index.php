<?php
/* @var $this AlertLogController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Alert Logs',
);

$this->menu=array(
	array('label'=>'Create AlertLog', 'url'=>array('create')),
	array('label'=>'Manage AlertLog', 'url'=>array('admin')),
);
?>

<h1>Alert Logs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
