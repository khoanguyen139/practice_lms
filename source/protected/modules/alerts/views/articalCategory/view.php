<?php
/* @var $this ArticalCategoryController */
/* @var $model ArticalCategory */

$this->breadcrumbs=array(
	'Artical Categories'=>array('index'),
	$model->article_category_id,
);

$this->menu=array(
	array('label'=>'List ArticalCategory', 'url'=>array('index')),
	array('label'=>'Create ArticalCategory', 'url'=>array('create')),
	array('label'=>'Update ArticalCategory', 'url'=>array('update', 'id'=>$model->article_category_id)),
	array('label'=>'Delete ArticalCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->article_category_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ArticalCategory', 'url'=>array('admin')),
);
?>

<h1>View ArticalCategory #<?php echo $model->article_category_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'article_category_id',
		'article_category_name',
	),
)); ?>
