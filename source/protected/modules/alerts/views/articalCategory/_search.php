<?php
/* @var $this ArticalCategoryController */
/* @var $model ArticalCategory */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'article_category_id'); ?>
		<?php echo $form->textField($model,'article_category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'article_category_name'); ?>
		<?php echo $form->textField($model,'article_category_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->