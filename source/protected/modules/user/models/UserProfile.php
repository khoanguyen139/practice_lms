<?php

/**
 * This is the model class for table "user_profile".
 *
 * The followings are the available columns in table 'user_profile':
 * @property integer $user_profile_id
 * @property integer $user_id
 * @property string $user_first_name
 * @property string $user_last_name
 * @property string $user_gender
 * @property string $user_photo
 * @property integer $user_dob
 * @property string $user_signature
 * @property string $user_signature_salt
 * @property string $user_signature_type
 * @property string $user_signature_size
 * @property string $user_signature_name
 * @property string $user_address
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserProfile extends CActiveRecord
{
    public $userImage;
    public $user_email;
    public $uploadedFile;

    public $currentPhoto;

    const IMAGE_HEIGHT = 80;
    const IMAGE_WIDTH = 80;

    const SIGNATURE_IMAGE_HEIGHT = 150;
    const SIGNATURE_IMAGE_WIDTH = 72;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('userImage, uploadedFile', 'file','types'=>'jpg, png', 'allowEmpty'=>true, 'on'=>'update'),
         	array('user_first_name, user_last_name, user_dob, user_phone, user_address', 'required', 'on'=>'profileUpdate'),
			array('user_id', 'required', 'on'=>'registration'),
			array('user_id, user_phone', 'numerical', 'integerOnly'=>true),
			array('user_photo', 'length', 'max'=>200),
			array('user_first_name, user_last_name', 'length', 'max'=>45),
            array('user_address', 'length', 'max'=>255),
			array('user_signature, user_dob', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_profile_id, user_id, user_first_name, user_last_name, user_gender, user_birthday, user_phone, user_address, user_photo, user_dob, user_signature', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'userLeaveApprove' => array(self::BELONGS_TO, 'Leave', 'user_id'),
            );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_profile_id' => 'User Profile',
			'user_id' => 'User',
			'user_first_name' => 'User First Name',
			'user_last_name' => 'User Last Name',
            'user_gender' => 'User Gender',
            'user_birthday' => 'User Birthday',
            'user_phone' => 'User Phone',
            'user_address' => 'User Address',
			'user_photo' => 'User Photo',
			'user_dob' => 'User Dob',
			'user_signature' => 'User Signature',
			'currentPhoto' => 'Current Photo',
		);
	}

    public function beforeSave()
    {
        $this->user_dob = strtotime($this->user_dob);
        return parent::beforeSave();
    }

    public function afterSave()
    {
        $this->user_dob = date('d-m-Y', $this->user_dob);
        return parent::afterSave();
    }

    protected function afterFind()
    {
        if(!empty($this->user_dob))
            $this->user_dob = date('d-m-Y',$this->user_dob);
        else
            $this->user_dob = '';

        $this->user_signature = base64_decode($this->user_signature);
        return parent::afterFind();
    }

     /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_profile_id',$this->user_profile_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_first_name',$this->user_first_name,true);
		$criteria->compare('user_last_name',$this->user_last_name,true);
		$criteria->compare('user_photo',$this->user_photo,true);
		$criteria->compare('user_dob',$this->user_dob);
		$criteria->compare('user_signature',$this->user_signature,true);
        $criteria->compare('user_gender', $this->user_gender, true);
        $criteria->compare('user_phone', $this->user_phone, true);
        $criteria->compare('user_address', $this->user_address, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
