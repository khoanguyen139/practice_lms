<?php
/* @var $this UserProfileController */
/* @var $data UserProfile */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_profile_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->user_profile_id), array('view', 'id'=>$data->user_profile_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_first_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_last_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_photo')); ?>:</b>
	<?php echo CHtml::encode($data->user_photo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_dob')); ?>:</b>
	<?php echo CHtml::encode($data->user_dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_signature')); ?>:</b>
	<?php echo CHtml::encode($data->user_signature); ?>
	<br />


</div>