<?php

class SiteController extends Controller
{
    public function SendMail()
    {
        $mail = new YiiMailer();
        $name = Yii::app()->user->getName();
        
        $email = Yii::app()->user->getEmail();
        $mail->setView('contact');
        $mail->setData(array('message' => 'Message to send', 'name' => 'John Doe', 'description' => 'Contact form'));
        $mail->setLayout('main');
        $mail->setFrom($email, $name);
        $mail->setTo(Yii::app()->params['adminEmail']);
        $mail->setSubject('Mail subject');
        $mail->setBody('Simple message');
        
        if ($mail->send()){
            Yii::app()->user->setFlash('contact','Thank you for contacting us.We will respond you as soon as possible.');
        } else {
            Yii::app()->user->setFlash('error','error while sending mail: '.$mail->getError());
        }
        
    }
    
    public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
    {
        if(Yii::app()->user->isGuest)
        {
            $this->redirect('user/login');
        }

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($baseUrl.'/css/listleavestyle.css');
        //get count
        $criteria = new CDbCriteria(array(
            'order' => 'created_at DESC',
        ));

        $pendingCount = 0;
        $pendingCriteria = new CDbCriteria();
        if (isset(user()->role)) {
            if (user()->role == User::ROLE_GENERAL) {
                $criteria->addCondition('requester_id=' .user()->id);
                $pendingCriteria->addCondition('status=' .Leave::STATUS_PENDING. ' AND requester_id=' .user()->id);
            } elseif (user()->role == User::ROLE_MANAGER) {
                $leaderMember = LeaderMember::model()->findAllByAttributes(array(
                    'leader_id' => user()->id,
                ));
                $criteria->addCondition('requester_id=' .user()->id);
                $pendingCriteria->addCondition('requester_id=' .user()->id);
                foreach ($leaderMember as $item) {
                    $criteria->addCondition('requester_id=' . $item->member_id, 'OR');
                    $pendingCriteria->addCondition('requester_id=' . $item->member_id, 'OR');
                }
                $pendingCriteria->addCondition('status=' .Leave::STATUS_PENDING);

            } else {
                $pendingCriteria->addCondition('status='.Leave::STATUS_PENDING);
            }
        }

        //$count = Leave::model()->count($criteria);


        $pendingCount = Leave::model()->count($pendingCriteria);

        //$pages = new CPagination($count);

        $pageSize = app()->getParams()['pageSize'];
        if (app()->request->isAjaxRequest) {
            if (app()->request->getParam('ajaxPageSize') != NULL) {
                $pageSize = app()->request->getParam('ajaxPageSize');
            }
        }

        $dataProvider = new CActiveDataProvider(
            'Leave',
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => $pageSize,
                ),
            ));

        $criteriaLatest = new CDbCriteria(array(
            'order' => 'created_at DESC',
            'limit' => 10,
        ));
        $dataProviderLatest = new CActiveDataProvider(
            'Leave',
            array(
                'criteria' => $criteriaLatest,
                'pagination' => false,
            ));

        $this->render('index',array(
                'dataProvider' => $dataProvider,
                'dataProviderLatest' => $dataProviderLatest,
                'pendingCount' => $pendingCount,
            )
        );
	}

    public function actionSortPending()
    {
        if(Yii::app()->user->isGuest)
        {
            $this->redirect('user/login');
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($baseUrl.'/css/listleavestyle.css');

        $pendingCriteria = new CDbCriteria(array(
            'order' => 'created_at DESC'
        ));

        $model = User::model()->findByAttributes(array(
            'user_id' => user()->id,
        ));

        if (isset(user()->role)) {
            if (user()->role == User::ROLE_GENERAL) {
                $pendingCriteria->addCondition('requester_id='.user()->id);
            } elseif (user()->role == User::ROLE_MANAGER) {
                $leaderMember = LeaderMember::model()->findAllByAttributes(array(
                    'leader_id' => user()->id,
                ));
                $pendingCriteria->addCondition('requester_id='.user()->id);
                foreach ($leaderMember as $item) {
                    $pendingCriteria->addCondition('requester_id=' .$item->member_id, 'OR');
                }
            }
        }
        $pendingCriteria->addCondition('status=' .Leave::STATUS_PENDING);
        $count = Leave::model()->count($pendingCriteria);

        $pageSize = app()->getParams()['pageSize'];
        if (app()->request->isAjaxRequest) {
            if (app()->request->getParam('ajaxPageSize') != NULL) {
                $pageSize = app()->request->getParam('ajaxPageSize');
            }
        }


        $dataProvider = new CActiveDataProvider(
            'Leave',
            array(
                    'criteria' => $pendingCriteria,
                    'pagination' => array(
                        'pageSize' => $pageSize,
                    ),
                 )
        );

        $criteriaLatest = new CDbCriteria(array(
            'order' => 'created_at DESC',
        ));
        $dataProviderLatest = new CActiveDataProvider(
            'Leave',
            array(
                'criteria' => $criteriaLatest,
            ));

        $this->render('index',array(
                'dataProvider' => $dataProvider,
                'dataProviderLatest' => $dataProviderLatest,
                'pagination' => array(
                    'pageSize' => 5,
                ),
                'pendingCount' => $count,
            ));
    }

    public function actionAboutUs()
    {
        $this->render('aboutUs');
    }

    public function actionManageEmployees()
    {
        $this->render('manageEmployees');
    }

    public function actionEmployees()
    {
        $this->render('employees');
    }

    public function actionManageView()
    {
        $this->render('manageView');
    }

    public function actionContact()
    {
        $model = new ContactForm();

        if(!Yii::app()->user->isGuest)
        {
            $user = User::model()->findByPk(Yii::app()->user->getId());
            $model->email = $user->user_email;
            $model->name = $user->userProfiles->user_first_name.' '.$user->userProfiles->user_first_name;
        }

        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];

            if($model->validate())
            {

                $message = 'Email: '.$model->email.' <br/>'.'Phone: '.$model->subject.' <br/>'.$model->body;

                $from = Yii::app()->params['adminEmail'];
                $name = Yii::app()->name;

                $mail = new YiiMailer();
                $mail->setFrom($from, $name);
                $mail->setTo(Yii::app()->params['salesEmail']);
                $mail->setSubject('Request for a demo/enquiry');
                $mail->setBody($message);

                $mail->IsSMTP();
                $mail->Host = Yii::app()->params['smtpHost'];
                $mail->Port = Yii::app()->params['smtpPort'];
                $mail->SMTPAuth = Yii::app()->params['smtpAuth'];
                $mail->Username = Yii::app()->params['smtpUser'];
                $mail->Password = Yii::app()->params['smtpPassword'];

                if ($mail->send()) {
                    Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                    $this->refresh();
                }

            }
        }

        $this->render('contact',array('model'=>$model));
    }

    /**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
        $error=Yii::app()->errorHandler->error;
		if($error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', array('error' => $error));
		}
	}

    public function actionGitPull()
    {
        if ($this->isAdmin()) {
            echo exec('whoami');
            $dateNow = date('dmY');

            echo "<br />";
            $path = "/home/lms/";
            chdir($path);
            echo exec('pwd');
            echo "<br />";
            echo "<pre>" . shell_exec('echo ---------------------' . date('d-m-Y H:i:s') . '-------------------------- >> git_log/pull_' . $dateNow);
            echo "<pre>" . shell_exec('git pull 2>&1 >> git_log/pull_' . $dateNow);
            echo "<br />";
            echo "<pre>" . file_get_contents("git_log/pull_" . $dateNow);
        } else {
            echo "You are not admin!";
        }
    }

}