<?php

return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'modules' => array(
            // uncomment the following to enable the Gii tool
            'gii'=>array(
                'class' => 'system.gii.GiiModule',
                'password' => '123',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters' => array('127.0.0.1','::1'),
            ),
        ),

        'components' => array(
            'db' => array(
                'connectionString' => 'mysql:host=127.0.0.1;dbname=lms_db',
                'emulatePrepare' => true,
                'username' => 'lms',
                'password' => 'qUyB6LsGRXKRnMWE', //'101tamimi',
                'charset' => 'utf8',
                'enableProfiling'=>true,
                'enableParamLogging' => true,
            ),
            'authManager'=>array(
                'class'=>'CDbAuthManager',
                'connectionID'=>'db',
            ),


/*            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error, warning',
                    ),
                    array(
                        'class' => 'application.extensions.pqp.PQPLogRoute',
                        'categories' => 'application.*, exception.*',
                    ),
                ),
            ),
*/
        ),
    )
);

?>