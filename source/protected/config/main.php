    <?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

require_once( dirname(__FILE__) . '/../components/helpers.php');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'CERES Leave Management System',
	// preloading 'log' component
	'preload'=>array('log'),

    //'theme'=>'classic',
    //'defaultController' => 'site',

    // autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.user.UserModule',
		'application.extensions.*',
        'application.extensions.ceres.components.*',
        'application.extensions.ceres.models.*',
        //'ext.YiiMailer.YiiMailer',
        //'ext.yiimail.YiiMailMessage'



	),

    'aliases' => array(
        'xupload' => 'ext.xupload'
    ),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

        'alerts',

        'user'=>array(
            'defaultController' => 'user',
            # send activation email
            //'sendActivationMail' => true,
            //'registrationUrl' => array('/user/registration'),
            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
            # login form path
            'loginUrl' => array('/user/login'),
            # page after login
            //'returnUrl' => array('/site/company'),
            # page after logout


        ),

	),

	// application components
	'components'=>array(
        'errorHandler'=>array(
            'errorAction'=>'site/error',
        ),
//        'request'=>array(
//            'enableCsrfValidation'=>true,
//        ),

		'user'=>array(
			// enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(

                //'aboutUs' => array('site/aboutUs'),
                'terms-conditions' => array('site/TermsConditions'),
                'privacy-policy' => array('site/PrivacyPolicy'),
                //'feedback' => array('site/contact'),
                //'article/details/<id:\d+>' => array('site/articleDetails'),
                //'articles' => array('site/articles'),
                'user/<_a:\w+>/<id:\d+>' =>'user/user/<_a>/<id>',
                'user/<_a:\w+>'          =>'user/user/<_a>',
                'user'                   =>'user/user',
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
        'session' => array (
            'autoStart' => false,
            'timeout' => 1200,
        ),

        ),
            
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
        'adminEmail'=>'register@wpsaeportal.com',
        'salesEmail'=>'sales@wpsaeportal.com',
        'smtpHost'=>'smtp.emailsrvr.com',
        'smtpPort'=>'25',
        'smtpAuth'=>true,
        'smtpUser'=>'register@wpsaeportal.com',
        'smtpPassword'=>'Test312',
        'anualLeave'=>'12',
        'weddingLeave'=>'3',
        'personalLeave'=>'3',
        'ceresMail'      => array(
            'host' => 'smtp.gmail.com',
            'port' => 587,
            'secure' => 'tls',
            //'auth' => false,
            'auth' => array(
                'email' => 'mailer@ceres.com.vn',
                'password' => 'ceres.m@iler'
            ),
            'auto' => array(
                'id' => '(auto)',
                'reply-to' => 'contact@lms.com',
                'from' => array('Lms', 'contact@lms.com'),
                'cc' => array(),
                'bcc' => array()
            ),
            'templates'     => array(
                'signup' => 1,
                'forgotPass' => 2,
                'leaveRecord' =>3,
            ),
            'emailsPerTime' => 10,
            'httpPass'      => '123456',
        ),
        'pageSize' => 5,
        'annual' => 12,
        'wedding' => 3,
        'personal' => 3,
	),
    
);