<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Error page</title>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic,600italic,700italic,700,800italic,800' rel='stylesheet' type='text/css'>
    <link href="<?php Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet">
    <link href="<?php Yii::app()->request->baseUrl; ?>/css/color.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php Yii::app()->request->baseUrl; ?>/js/vendor/html5shiv.js"></script>
    <script src="<?php Yii::app()->request->baseUrl; ?>/js/vendor/respond.min.js"></script>
    <![endif]-->
    <!--[if lte IE 7]>
    <script src="<?php Yii::app()->request->baseUrl; ?>/js/vendor/lte-ie7.js"></script><![endif]-->
</head>
<body id="error">
<div class="error">
    <div class="page-header"><span>404</span><span class="icon icon-warning"></span></div>
    <p>Oops! Sorry, that page could'nt be found.</p>
    <form class="form-inline dropdown clearfix">
        <input type="text" class="form-control dropdown-toggle" data-toggle="dropdown" placeholder="Search a site..">
        <button type="submit" class="btn btn-search"><span class="glyphicon glyphicon-search"></span></button>
    </form>
    <a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-arrow-left"></span>Back</a>
    <div id="content">

    </div>
</div><!--End login-->
<script src="<?php Yii::app()->request->baseUrl; ?>/js/vendor/jquery.js"></script>
<script src="<?php Yii::app()->request->baseUrl; ?>/js/vendor/bootstrap.min.js"></script>
</body>
</html>
