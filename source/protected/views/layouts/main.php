<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic,600italic,700italic,700,800italic,800' rel='stylesheet' type='text/css'>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/color.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/html5shiv.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>//js/vendor/respond.min.js"></script>
        <![endif]-->
        <!--[if lte IE 7]><script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/lte-ie7.js"></script><![endif]-->
    </head>
    <body id="index">
        <header id="header" class="navbar navbar-default navbar-fixed-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/site/index"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo-top.png" alt="">Ceres Leave Record</a>
            </div><!--home button-->

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="circle" href="<?php echo url('/');?>">
                            <span class="glyphicon glyphicon-dashboard"></span>
                            <span class="text">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a class="circle" href="/leave/create">
                            <span class="glyphicon glyphicon-plus"></span>
                            <span class="text">Add New Leave</span>
                        </a>
                    </li>
                    <?php if (isset(user()->role) && user()->role == User::ROLE_ADMIN) {?>
                        <li>
                            <a class="circle" href="/employee">
                                <span class="glyphicon glyphicon-list-alt"></span>
                                <span class="text">List Employees</span>
                            </a>
                        </li>
                    <?php }?>

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="notification">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-envelope"></span>
                            <?php
                                $leave = new Leave();
                                $leave->requester_id = user()->id;
                            ?>
                            <?php if ($leave->getPendingLeaveCount() > 0) : ?>
                                <span class="badge"><?php echo CHtml::encode($leave->getPendingLeaveCount()); ?></span>
                            <?php endif; ?>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo Yii::app()->controller->createUrl("/leave/index", array("id" => 1)); ?>">
                                    <div class="pull-left">
                                        <img class="avatar" alt="" src="/img/avatar2.jpg">
                                    </div>
                                    <div class="pull-right">
                                        <strong class="author">Mary Lee</strong>
                                        <span class="content">Lorem Ipsum is simply dummy text of the...</span>
                                        <em class="time">12/12/2013</em>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->controller->createUrl("/leave/index", array("id" => 2)); ?>">
                                    <div class="pull-left">
                                        <img class="avatar" alt="" src="img/avatar1.jpg">
                                    </div>
                                    <div class="pull-right">
                                        <strong class="author">Anna Lee</strong>
                                        <span class="content">Lorem Ipsum is simply dummy text of the...</span>
                                        <em class="time">12/12/2013</em>
                                    </div>
                                </a>
                            </li>
                            <li><a href="/site/index">Messages center</a></li>
                        </ul>
                    </li><!--notification-->
                    
                    <li class="account dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img class="avatar" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/img/avatar-default.png">
                            <span>
                                <?php
                                $userProfile = UserProfile::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
                                echo $userProfile->getAttribute('user_first_name') . " " . $userProfile->getAttribute('user_last_name')
                                ?>
                            </span>
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/user/userProfile/detail"><span class="glyphicon glyphicon-user"></span>My Profile</a></li>
                            <li><a href="/user/newPassword"><span class="glyphicon glyphicon-edit"></span>Change password</a></li>
                        </ul>
                    </li>
                    <li class="logout">
                        <a href="/user/logout" class="navbar-link"><span class="glyphicon glyphicon-log-out"></span>Logout</a>
                    </li>
                </ul>
            </div><!--navigator bar-->

        </header><!--header-->

        <div id="content" class="manage">
            <?php echo $content; ?>
        </div><!--main content-->

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/bootstrap.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/bootstrap-datepicker.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/application.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
    </body>
</html>