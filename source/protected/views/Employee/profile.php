<?php
/* @var $this UserProfileController */
/* @var $model UserProfile */
/* @var $model User */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Profile';
$this->breadcrumbs = array(
    'Profile',
);
?>
<head>
    <title>Update Profile</title>
</head>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => false,
    ),
    'htmlOptions' => array(
        'class' => 'form-horizontal clearfix',
        'enctype' => 'multipart/form-data'
        )));
?>

<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>




<section class="col-lg-12">

    <h1 class="title">Update Profile's <?php echo $model->user->user_name; ?></h1>

    <form class="form-horizontal col-lg-6" role="form"> 
        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'user_first_name', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php
                echo $form->textField($model, 'user_first_name', array(
                    'class' => 'form-control',
                    'placeholder' => 'First Name',
                        //'style' => 'height:35px; width:307px',
                ));
                ?>

                <?php echo $form->error($model, 'user_first_name'); ?>
            </div>
        </div>    


        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'user_last_name', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php
                echo $form->textField($model, 'user_last_name', array(
                    'class' => 'form-control',
                    'placeholder' => 'Last Name',
                    'style' => 'height:35px; width:307px',
                ));
                ?>

                <?php echo $form->error($model, 'user_last_name'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'user_gender', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php
                echo $form->radioButton($model, 'user_gender', array(
                    'value' => "Male",
                    'uncheckValue' => null,
                    'style' => "margin-right: 10px;",
                ));
                echo "Male";

                echo $form->radioButton($model, 'user_gender', array(
                    'value' => "Female",
                    'uncheckValue' => null,
                    'style' => "margin-right: 10px; margin-left: 10px;",
                ));
                echo "Female";

                echo $form->radioButton($model, 'user_gender', array(
                    'value' => "Unknown",
                    'uncheckValue' => null,
                    'style' => "margin-right: 10px; margin-left: 10px;",
                ));
                echo "Other";
                ?>

                <?php echo $form->error($model, 'user_gender'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'user_dob', array('class' => 'col-lg-2 control-label')); ?>
            <div class="col-lg-3">
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'user_dob',
                    'model' => $model,
                    'attribute' => 'user_dob',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'dd-mm-yy',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1920:' . (date('Y') - 18),
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-control',
                        'style' => 'height:35px; width:307px',
                        'value' => date('d/m/Y', strtotime($model->user_birthday)),
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'user_dob'); ?>
            </div>
        </div>

<!--        <div class="form-group">
            //<?php
//            echo $form->labelEx($model, 'user_email', array(
//                'class' => 'col-lg-2 control-label'
//            ));
//            ?>
            <div class="col-lg-3">
                //<?php
//                echo $form->textField($model, 'user_email', array(
//                    'class' => 'form-control',
//                    'placeholder' => 'Email',
//                    'style' => 'height:35px; width:307px',
//                ));
//                ?>

                //<?php //echo $form->error($model, 'user_email'); ?>
            </div>
        </div>-->

        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'user_phone', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php
                echo $form->textField($model, 'user_phone', array(
                    'class' => 'form-control',
                    'placeholder' => 'Phone Number',
                    'style' => 'height:35px; width:307px',
                ));
                ?>

                <?php echo $form->error($model, 'user_phone'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'user_address', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php
                echo $form->textField($model, 'user_address', array(
                    'class' => 'form-control',
                    'placeholder' => 'Address',
                    'style' => 'height:35px; width:307px',
                ));
                ?>

                <?php echo $form->error($model, 'user_address'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'currentPhoto', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php
                $path = Yii::app()->request->baseUrl . '/images/';
                (!empty($model->user_photo)) ? $path .= 'user/'. $model->user_photo : $path .= 'no_avatar.jpg';
                echo CHtml::image( $path, $model->user_photo, array(
                    "width" => "100px", "height" => "100px"
                ));
                ?>
            </div>
        </div>

        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'change_photo', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php
                echo $form->fileField($model, 'userImage', array(
                    'class' => 'form-control',
                    'placeholder' => 'Signature',
                    'style' => 'border-width: 0px;'
                ));
                ?>
            </div>
        </div>

        <?php if (Yii::app()->user->checkAccess('employer') || Yii::app()->user->checkAccess('approver')): ?>
            <div class="form-group">
                <?php
                echo $form->labelEx($model, 'user_signature', array(
                    'class' => 'col-lg-2 control-label'
                ));
                ?>
                <div class="col-lg-3">
                    <?php
                    echo $form->fileField($model, 'uploadedFile', array(
                        'class' => 'form-control',
                        'placeholder' => 'Signature'
                    ));
                    ?>
                    <?php echo $form->error($model, 'user_signature'); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <?php
            echo $form->labelEx($model, 'Role', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-3">
                <?php echo CHtml::dropDownList('role', $model->user->user_role_id, array(
                    User::ROLE_ADMIN => 'Admin',
                    User::ROLE_MANAGER => 'Manager',
                    User::ROLE_GENERAL => 'General'
                ))?>
            </div>
        </div>
        <?php if ($model->user->user_role_id == User::ROLE_MANAGER) {?>
            <?php
                $freeMemberUser = User::model()->getFreeMember();
            ?>
            <div class="form-group">
                <div>
                    <label class="col-lg-2">Free Members</label>
                </div>
                <div class="col-lg-3">
                    <?php
                    echo CHtml::dropDownList(
                        'free_member_id',
                        '',
                        CHtml::listData($freeMemberUser, 'user_id', 'user_name'),
                        array(
                            'multiple' => true,
                            'style' => 'height: 70px;'
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-lg-2">&nbsp;</div>
                <div class="col-lg-6">
                    <a class="btn btn-small btn-success" style="border-radius: 3px;" id="add-all-members"
                       onclick="addMembers(<?php echo $model->user_id; ?>)">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add Selected Members
                    </a>
                    <a class="btn btn-small btn-primary" style="border-radius: 3px;" id="add-all-members"
                       onclick="addAllMembers(<?php echo $model->user_id; ?>)">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add All Members
                    </a>
                </div>
            </div>
            <?php
                $memberOfMeUser = User::model()->getMemberOfMe($model->user_id);
            ?>
            <div class="form-group">
                <div  class="col-lg-2">Current Members</div>
                <div class ="col-lg-6 data-member">
                    <?php
                    if ($memberOfMeUser) {
                        $display = 'block';
                        foreach ($memberOfMeUser as $item) { ?>
                            <div class="item" style="margin-bottom: 10px; margin-top: 10px;" id="item-<?php echo $item->user_id;?>" >
                                <a class="btn btn-small btn-danger" style="border-radius: 3px; margin-right: 10px;" onclick="removeMember(<?php echo $item->user_id; ?>)">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                                <span><?php echo $item->user_name; ?></span>
                            </div>
                        <?php
                        }
                    } else {
                        $display = 'none';
                        echo '<p>Not found member</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="form-group" id="remove-all-members" style="display: <?php echo $display; ?>">
                <div  class="col-lg-2">&nbsp;</div>
                <div class="col-lg-6">
                    <div class="controls" rel="<?php echo $model->user_id; ?>">
                        <a class="btn btn-small btn-danger" style="border-radius: 3px; margin-bottom: 10px;"onclick="removeAllMembers(<?php echo $model->user_id; ?>)">
                            <i class="glyphicon glyphicon-trash"></i>
                            Remove All Members
                        </a>
                    </div>
                </div>
            </div>
        <?php }?>

        <div class="form-group">
            <?php
            echo $form->labelEx($model, '', array(
                'class' => 'col-lg-2 control-label'
            ));
            ?>
            <div class="col-lg-6">
                <?php echo CHtml::submitButton('Update', array('class' => 'btn-success')); ?>
                <?php echo CHtml::resetButton('Reset', array('class' => 'btn-default')); ?>
            </div>
        </div>    

        <?php $this->endWidget(); ?>
    </form>
</section>
<script>
    function addMembers(leader_id)
    {
        var memberList = [];
        $("#free_member_id > option:selected").each(function () {
            memberList.push($(this).val());
        });

        if (memberList != '') {
            $.ajax({
                url : '<?php echo url('/employee/addallmembers') ?>',
                type: 'post',
                data: {leader_id : leader_id, free_member_id : memberList.join(",")},
                dataType: 'json',
                success : function(data) {
                    if (data) {
                        dataView(data);
                    }
                }
            });
        } else
            alert ('Please select members!');
        return true;
    }

    function addAllMembers(leader_id)
    {
        var memberList = [];
        $("#free_member_id > option").each(function () {
            memberList.push($(this).val());
        });
        if (memberList != '') {
            $.ajax({
                url : '<?php echo url('/employee/addallmembers') ?>',
                type: 'post',
                data: {leader_id : leader_id, free_member_id: memberList.join(",")},
                dataType: 'json',
                success : function(data) {
                    if (data) {
                        dataView(data);
                    }
                }
            });
            return true;
        } else {
            alert ('The member list is empty!');
        }
        return true;
    }

    function dataView(data)
    {
        var leaderMember = eval(data);
        for (var i = 0; i < leaderMember.length; i++) {
            var icon = "<a class='btn btn-small btn-danger' style='border-radius: 3px; margin-right: 10px;' onclick='removeMember(" + leaderMember[i][0] + ")' ><i class='glyphicon glyphicon-trash'></i></a>";
            $('.data-member').append("<div class='item' style='margin-bottom: 10px; margin-top: 10px;' id='item-" + leaderMember[i][0] + "'>" + icon + " <span>" + leaderMember[i][1] + "</span></div>");
        }
        $('.data-member p').html('');
        $('#remove-all-members').show();
    }

    function removeMember(member_id)
    {
        var leader_id = <?php echo $model->user_id; ?>;
        if (confirm('Do you want delete this member?')) {
            $.ajax({
                url : '<?php echo url('/employee/removemember') ?>',
                type: 'post',
                data: {leader_id: leader_id, member_id : member_id},
                dataType: 'json',
                success : function(data) {
                    if (data) {
                        if (data == 2) {
                            $('.data-member').html('<p>Not found member</p>');
                            $('#remove-all-members').hide();
                        }
                        $('#item-' + member_id).hide(500);
                        return true;
                    } else {
                        alert('Delete error.');
                    }
                }
            });
        }
        return true;
    }

    function removeAllMembers(leader_id)
    {
        if (confirm('Do you want remove all members?')) {
            $.ajax({
                url : '<?php echo url('/employee/removeallmembers') ?>',
                type: 'post',
                data: {leader_id : leader_id},
                success : function(data) {
                    if (data) {
                        $('.item').hide(500);
                        $('.data-member').html('<p>Not found member</p>');
                        $('#remove-all-members').hide();
                    }
                }
            });
            return true;
        }
    }
</script>
