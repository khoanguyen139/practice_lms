<?php
/* @var $userModel EmployeeController */
/* @var $roleList EmployeeController */
/* @var $statusList EmployeeController */
$this->breadcrumbs = array(
    'Add User',
);
;?>
<head>
    <title>Add new User</title>
</head>
<?php
$form = $this->beginWidget('CActiveForm',
    array(
        'id' => 'addUser-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => 'form-horizontal clearfix',
            'enctype' => 'multipart/form-data'
        )
    )
)
;?>

<?php if(Yii::app()->user->hasFlash('success')):;?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close"></button>
        <?php echo Yii::app()->user->getFlash('success');?>
    </div>
<?php endif;?>

<section class="col-lg-12">
    <h1 class="title">Add new User</h1>

    <form class="form-horizontal col-lg-6" role="form">
        <div class="form-group">
            <?php echo $form->labelEx($userModel, 'user_status', array(
                'class' => 'col-lg-2 control-label'
            ));?>
            <div class="col-lg-3">
                <?php
                    echo $form->dropDownList($userModel, 'user_status',
                        array('Active' => 'Active', 'Inactive' => 'Inactive'),
                        array(
                        'class' => 'form-control',
                        'style' => 'height: 35px; width: 280px',
                        'options' => array(
                            'Active' => array('selected' => true),
                            'Inactive' => array('selected' => false)
                            )
                    ))
                ;?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($userModel, 'user_role_id', array(
                'class' => 'col-lg-2 control-label'
            ));?>
            <div class="col-lg-3">
                <?php echo $form->dropDownList($userModel, 'user_role_id', $roleList, array(
                        'class' => 'form-control',
                        'style' => 'height: 35px; width: 280px',
                        'options' => array(
                            '4' => array('selected' => true)
                        )
                    )
                );?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($userModel, 'user_name', array(
                'class' => 'col-lg-2 control-label'));?>
            <div class="col-lg-3">
                <?php echo $form->textField($userModel, 'user_name', array(
                    'class' => 'form-control',
                    'placeholder' => 'Enter Username',
                    'style' => 'height: 35px; width: 280px'
                ));?>
            </div>
            <?php echo $form->error($userModel, 'user_name');?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($userModel, 'user_password', array(
                'class' => 'col-lg-2 control-label'
            ));?>
            <div class="col-lg-3">
                <?php echo $form->passwordField($userModel, 'user_password', array(
                    'class' => 'form-control',
                    'style' => 'height: 35px; width: 280px',
                    'placeholder' => 'Enter Password'
                ));?>
            </div>
            <?php echo $form->error($userModel, 'user_password');?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($userModel, 'verifyPassword', array(
                'class' => 'col-lg-2 control-label'
            ));?>
            <div class="col-lg-3">
                <?php echo $form->passwordField($userModel, 'verifyPassword', array(
                    'class' => 'form-control',
                    'style' => 'height: 35px; width: 280px',
                    'placeholder' => 'Re-enter Password'
                ));?>
            </div>
            <?php echo $form->error($userModel, 'verifyPassword');?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($userModel, 'user_email', array(
                'class' => 'col-lg-2 control-label'
            ));?>
            <div class="col-lg-3">
                <?php echo $form->textField($userModel, 'user_email', array(
                    'class' => 'form-control',
                    'style' => 'height: 35px; width: 280px',
                    'placeholder' => 'Enter Email Address'
                ))
                ;?>
            </div>
            <?php echo $form->error($userModel, 'user_email')?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($userModel, '', array(
                'class' => 'col-lg-2 control-label'
            ));?>
            <div class="col-lg-3">
                <?php echo CHtml::submitButton('Submit', array('class' => 'btn-success')) ;?>
                <?php echo CHtml::resetButton('Reset', array('class' => 'btn-default')) ;?>
            </div>
        </div>
    </form>
    <?php $this->endWidget();?>

</section>

<?php
/*
    echo $form->labelEx($model, 'user_first_name', array(
        'class' => 'col-lg-2 control-label'
    ));

    echo $form->textField($model, 'user_first_name', array(
        'class' => 'form-control',
        'placeholder' => 'First Name',
        'style' => 'height:35px; width:280px',
    ));

     echo $form->error($model, 'user_first_name');
*/
?>