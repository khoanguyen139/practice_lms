    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="pull-left modal-title">
                    Leave Record for
                    <span>
                        <?php
                        echo $modelLeave->getNameRequester();
                        ?>
                    </span>
                </h2>

                <div class="pull-right">
                    <ul class="nav-ul">
                        <li>
                            <a class="circle" href="#">
                                <span class="glyphicon glyphicon-print"></span>
                                <span class="text">Print</span>
                            </a>
                        </li>
                        <li>
                            <a class="circle" href="savepdf">
                                <span class="glyphicon glyphicon-saved"></span>
                                <span class="text">Save as PDF</span>
                            </a>
                        </li>
                        <li>
                            <a class="circle" href="#" aria-hidden="true" data-dismiss="modal" type="button">
                                <span class="glyphicon glyphicon-remove"></span>
                                <span class="text">Close</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12 modal-body clearfix">

                <div class="row">
                    <div class="row-small">
                        <span class="col-lg-4 field-name">Leave Type</span>
            <span class="col-lg-8">
                <?php
                echo $modelLeave->category->name;
                ?>
            </span>
                    </div>
                    <div class="row-small">
                        <span class="col-lg-4 field-name">Time</span>
            <span class="col-lg-8">
                <?php
                $fromTime = date_create($modelLeave->from_time);
                echo date_format($fromTime, 'd/m/Y');
                ?>
            </span>
                    </div>
                    <div class="row-small">
                        <span class="col-lg-4 field-name">Working Days Applied</span>
            <span class="col-lg-8">
                <?php
                echo $modelLeave->applied_days;
                ?>
            </span>
                    </div>
                    <div class="row-small">
                        <span class="col-lg-4 field-name">Approving Officer</span>
            <span class="col-lg-8">
                <?php
                echo $modelLeave->getNameApprover();
                ?>
            </span>
                    </div>
                    <div class="row-small">
                        <span class="col-lg-4 field-name">CC to other</span>
            <span class="col-lg-8">
                <?php
                echo $modelLeave->cc_email;
                ?>
            </span>
                    </div>
                    <div class="row-small">
                        <span class="col-lg-4 field-name">Address While On Leave</span>
            <span class="col-lg-8">
                <?php
                echo $modelLeave->wol_address;
                ?>
            </span>
                    </div>
                    <div class="row-small">
                        <span class="col-lg-4 field-name">Remark:</span>
                <span class="col-lg-8">
                    <?php
                    echo $modelLeave->reason;
                    ?>
                </span>

                    </div>
                </div>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

