<?php
class UserSummary extends CWidget
{
    public $userId;

    public function run()
    {

        $modelSummary = new Leave();

        $leaveSummariesByUser = $modelSummary->getLeaveSummariesByUser($this->userId);

        $this->render('_summary', array(
            'leaveSummariesByUser' => $leaveSummariesByUser,
        ));

    }
} 