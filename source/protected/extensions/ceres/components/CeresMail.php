<?php

class CeresMail
{
    /**
     * Add email to queue. Will auto support below params (into $params['data']) if missing:
     *  FROM_NAME
     *  FROM_EMAIL
     *  TO_NAME
     *  TO_EMAIL
     *
     * @param string $name Name of the mail template
     * @param array $params Data for the template
     *  Format: array(
     *      'to' => array('AAA;BBB', 'a@ceres.com.vn;b@ceres.com.vn'),
     *      'attachments' => array(
     *          'path/to/file-1',
     *          'path/to/file-2',
     *      ),
     *      'data' => array(
     *          'EXAMPLE' => 'Test',
     *      ),
     *  )
     */

    public static function queue($name, $params, $type = EmailQueue::TYPE_NORMAL)
    {
        $configs = self::getConfigs();
        $template = EmailTemplate::model()->findByPk($configs['templates'][$name]);
        self::queueFromTemplate($template, $params, $type);
    }


    /**
     * Create email queues from a template
     *
     * @param EmailTemplate $template an instance of the EmailTemplate class
     * @param Array $params data will be parsed into the template
     */
    public static function queueFromTemplate($template, $params, $type)
    {
        $configs = self::getConfigs();
        //var_dump($configs); die;
        $params = self::standardizeParams($params);
        //var_dump($params); die;
        if (!empty($template)) {
            $autoStr = $configs['auto']['id'];

            $queue = new EmailQueue();
            $queue->type = $type;
            $queue->template_id = $template->id;

            // From
            if (!isset($params['from'])) {
                $fromName = self::applyTemplateData(trim($template->from_name), $params['data']);
                $fromEmail = self::applyTemplateData(trim($template->from_email), $params['data']);
                if (empty($fromEmail) || $fromEmail == $autoStr) {
                    $fromName = $configs['auto']['from'][0];
                    $fromEmail = $configs['auto']['from'][1];
                } else {
                    if (empty($fromName))
                        $fromName = $fromEmail;
                }

            } else {
                $fromName = $params['from'][0];
                $fromEmail = $params['from'][1];
            }

            $queue->from_name = $fromName;
            $queue->from_email = $fromEmail;
            if (!isset($params['FROM_NAME'])) {
                $params['data']['FROM_NAME'] = $fromName;
                $params['data']['FROM_EMAIL'] = $fromEmail;
            }

            // To
            $toName = self::applyTemplateData(trim($template->to_name), $params['data']);
            $toEmail = self::applyTemplateData(trim($template->to_email), $params['data']);
            if (empty($toEmail) || $toEmail == $autoStr) {
                $toName = $params['to'][0];
                $toEmail = $params['to'][1];
            }
            $queue->to_name = $toName;
            $queue->to_email = $toEmail;
            if (!isset($params['TO_NAME'])) {
                $params['data']['TO_NAME'] = $toName;
                $params['data']['TO_EMAIL'] = $toEmail;
            }

            // Reply-to
            if ($queue->from_email == '') {
                $replyEmail = self::applyTemplateData(trim($template->reply_to), $params['data']);
                if ($replyEmail == $autoStr) {
                    if (isset($configs['auto']['reply-to']))
                        $replyEmail = $configs['auto']['reply-to'];
                    else $replyEmail = '';
                }
                $queue->reply_to = $replyEmail;
            } else {
                $queue->reply_to = $queue->from_email;
            }
            //CC
            $toEmailCC = self::applyTemplateData(trim($template->to_email), $params['data']);
            if (empty($toEmailCC) || $toEmail == $toEmailCC) {
                $toEmailCC = $params['cc'];
            }
            $queue->cc = $toEmailCC;


            // Subject
            $queue->subject = self::applyTemplateData($template->subject, $params['data']);
            $queue->content = self::applyTemplateData($template->content, $params['data']);

            $queue->created_time = time();
            $delayTime = $template->timeframe->value;
            $queue->send_time = $queue->created_time + ((int)$delayTime * 60);
            $queue->status = EmailQueue::STATUS_NOT_SENT;

            //var_dump($queue->save()); die;
            if ($queue->save() && isset($params['attachments'])) {
             //   die;
                if ($type == EmailQueue::TYPE_URGENT) {
                    $configs = param('ceresMail');
                    $queue->status = EmailQueue::STATUS_SENDING;
                    $queue->save(false);
                    if (isset($configs['mandrillApiKey']) && $configs['mandrillApiKey'] != '') {
                        CeresMail::sendByMandrill($configs['mandrillApiKey'], $queue);
                    } else {
                        CeresMail::send($queue);
                    }
                }
                // Attachments
                foreach ($params['attachments'] as $file) {
                    $att = new EmailAttachment();
                    $att->queue_id = $queue->id;
                    $att->file_path = $file;

                    $att->save();
                }
            }
        }
    }

    /**
     * Send emails in the queue
     *
     * @param EmailQueue $email An email in the queue
     * @return Bool Fail or success
     */
    public static function send($email)
    {
        if (!class_exists('PHPMailer', false)) {
            Yii::import('application.extensions.ceres.vendors.PHPMailer.*');
            require_once('class.phpmailer.php');
        }

        $mailer = new PHPMailer(true);
        // $mailer->SMTPDebug = 2;
        $mailer->IsSMTP();
        $mailer->CharSet = 'UTF-8';

        try {
            $configs = self::getConfigs();
            $mailer->Host = $configs['host'];
            
            if ($configs['secure'] != false)
                $mailer->SMTPSecure = $configs['secure'];
            
            $mailer->Port = $configs['port'];

            if ($configs['auth']) {
                $mailer->SMTPAuth = true;
                $mailer->Username = $configs['auth']['email'];
                $mailer->Password = $configs['auth']['password'];
            }

            // From and Reply-To
            if (empty($email->reply_to)) {
                $mailer->SetFrom($email->from_email, $email->from_name); // PHPMailer will auto add this email for Reply-To
            } else {
                $mailer->SetFrom($email->from_email, $email->from_name, false);
                $mailer->AddReplyTo($email->reply_to);
            }

            $mailer->Subject = $email->subject;
            $mailer->AltBody = strip_tags($email->content);
            $mailer->MsgHTML($email->content);

            // Receivers
            $delimiter = ';';
            $to_names = explode($delimiter, $email->to_name);
            $to_emails = explode($delimiter, $email->to_email);
            foreach ($to_emails as $index => $e) {
                $mailer->AddAddress($e, $to_names[$index]);
            }

            // Cc
            if (!empty($email->cc)) {
                $cc = explode($delimiter, $email->cc);
                foreach ($cc as $e)
                    $mailer->AddCC($e);
            }

            // Bcc
            if (!empty($email->bcc)) {
                $bcc = explode($delimiter, $email->bcc);
                foreach ($bcc as $e)
                    $mailer->AddBCC($e);
            }

            // Attachments
            if (!empty($email->attachments)) {
                foreach ($email->attachments as $file)
                    $mailer->AddAttachment($file);
            }

            $rs = $mailer->Send();
            if ($rs && $email->type == EmailQueue::TYPE_URGENT) {
                $email->sent_time = time();
                $email->status = EmailQueue::STATUS_SENT_SUCCESS;
                $email->save(false);
            }
            return $rs;
        } catch (phpmailerException $e) {
            var_dump($e->errorMessage());
        } catch (Exception $e) {
            var_dump($e->errorMessage());
        }

        return false;
    }

    /**
     * send email though ManDrill API
     * @param $key
     * @param $email
     * @return bool
     */
    public static function sendByMandrill ($key, $email)
    {
        $uri = 'https://mandrillapp.com/api/1.0/messages/send.json';

        // Recipients
        $delimiter = ';';
        $to_names = explode($delimiter, $email->to_name);
        $to_emails = explode($delimiter, $email->to_email);
        $recipients = array();
        foreach ($to_emails as $index => $e) {
            $recipients[] = array(
                "email" => $e,
                "name" => $to_names[$index]
            );
        }

        //CC & BCC
        $template = $email->template;
        if (!$template) {
            die('No templates match template_id of email #' . $email->id);
        }
        $ccEmails = (strpos($template->cc, ';') ? explode($delimiter, $template->cc) : array(0 => $template->cc));
        $bccEmails = (strpos($template->bcc, ';') ? explode($delimiter, $template->bcc) : array(0 => $template->bcc));
        foreach ($ccEmails as &$ccEmail) {
            $ccEmail = array('address' => $ccEmail, 'type' => 'cc');
        }
        foreach ($bccEmails as &$bccEmail) {
            $bccEmail = array('address' => $bccEmail, 'type' => 'bcc');
        }
        $minorEmails = array_merge($ccEmails, $bccEmails);

        foreach ($minorEmails as $minorEmail) {
            if (empty($minorEmail['address'])) {
                continue;
            }
            $emailParts = (strpos($minorEmail['address'], '|') ? explode('|', $minorEmail['address']) : array(0 => null, 1 => $minorEmail['address']));
            $name = trim($emailParts[0]);
            $emailAddress = trim($emailParts[1]);
            $emailValidator = new CEmailValidator;
            if (!$emailValidator->validateValue($emailAddress)) {
                continue;
            }
            $recipients[] = array(
                'email' => $emailAddress,
                'name' => $name,
                'type' => $minorEmail['type']
            );
        }

        //attach files in table `email_attachments`
        $attachments = EmailQueue::model()->findByPk($email->id)->attachments;
        $attachmentArray = array();
        foreach ($attachments as $attachment) {
            $fileContent = file_get_contents($attachment->file_path);
            $attachment_encoded = base64_encode($fileContent);
            $attachmentArray[] = array(
                'content' => $attachment_encoded,
                'name' => basename($attachment->file_path)
            );
        }

        $postString = array(
            "key" => $key,
            "message" => array(
                "html" => $email->content,
                "text" => strip_tags($email->content),
                "subject" => $email->subject,
                "from_email" => $email->from_email,
                "from_name" => $email->from_name,
                "to" => $recipients,
                "headers" => array(),
                "track_opens" => true,
                "track_clicks" => true,
                "auto_text" => true,
                "url_strip_qs" => true,
                "preserve_recipients" => true,

                "merge" => true,
                "global_merge_vars" => array(),
                "merge_vars" => array(),
                "tags" => array(),
                "google_analytics_domains" => array(),
                "google_analytics_campaign" => "...",
                "metadata" => array(),
                "recipient_metadata" => array(),
                "attachments" => $attachmentArray
            ),
            "async" => false
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postString));

        $result = curl_exec($ch);
        $return = json_decode($result);

        if (is_array($return) && isset($return[0])) {
            $object = $return[0];
            if ($object->status == 'sent' || $object->status == 'queued') {
                if ($template->delete_if_success == 1) {
                    if (!EmailAttachment::model()->deleteAllByAttributes(array('queue_id' => $email->id))) {
                        echo 'Cannot delete attached files of email #' . $email->id;
                    }
                }
                if ($email->type == EmailQueue::TYPE_URGENT) {
                    $email->sent_time = time();
                    $email->status = EmailQueue::STATUS_SENT_SUCCESS;
                    $email->save(false);
                }
                return true;
            }
        }

        if ($email->type == EmailQueue::TYPE_URGENT) {
            $email->status = EmailQueue::STATUS_SENT_FAIL;
            $email->save(false);
        }
        return false;
    }

    public static function applyTemplateData($data, $params)
    {
        $data = mb_convert_encoding($data, 'UTF-8', mb_detect_encoding($data, 'UTF-8, ISO-8859-1', true));

        $matches = array('');
        while (count($matches) > 0) {
            if (preg_match("/\[([a-zA-Z0-9_])*\]/", $data, $matches)) {
                $key = $matches[0];
                $property = substr($key, 1, strlen($key) - 2);
                if ($property != '') {
                    //var_dump($params); die;
                    $value = $params[$property];

                    $data = str_replace($key, $value, $data);
                }
            }
        }

        return $data;
    }

    public static function standardizeParams($params)
    {
        if (!isset($params['to']))
            $params['to'] = '';
        if (!isset($params['attachments']))
            $params['attachments'] = array();
        if (!isset($params['data']))
            $params['data'] = array();
        if (!isset($params['cc'])) {
            $params['cc'] = '';
        }
        return $params;
    }

    public static function getConfigs()
    {
        return param('ceresMail');
    }
}