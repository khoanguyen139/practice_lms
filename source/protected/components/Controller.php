<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to speci property.
	 */
	public $breadcrumbs=array();

    public function isAdmin() {
        if (isset(user()->role) && user()->role == User::ROLE_ADMIN) {
            return true;
        }
        return false;
    }

    public function isManager() {
        if (isset(user()->role) && user()->role == User::ROLE_MANAGER) {
            return true;
        }
        return false;
    }

    public function isGeneral() {
        if (isset(user()->role) && user()->role == User::ROLE_GENERAL) {
            return true;
        }
        return false;
    }

}
