<?php

/**
 * @author Phạm Dương Trần
 * @copyright kame
 * @email: pdtran1808@gmail.com
 *
 */
class lmsPager extends CLinkPager
{
    public function init()
    {
        parent::init();
        $app = Yii::app();

        //config paging params
        $this->header = '';
        $this->pageSize = 5;
        //$this->maxButtonCount = 0;
        $this->firstPageLabel = "First";
        //$this->nextPageLabel = '&raquo;';
        $this->prevPageLabel = "<span class=\"glyphicon glyphicon-chevron-left\"></span>";
        $this->nextPageLabel = "<span class=\"glyphicon glyphicon-chevron-right\"></span>";
        //$this->prevPageLabel = '&laquo;';
        $this->lastPageLabel = "Last";
        $this->htmlOptions['class'] = 'pages';
    }

    /**
     * Override parent function
     * Creates a page button.
     * You may override this method to customize the page buttons.
     *
     * @param string the text label for the button
     * @param integer the page number
     * @param string the CSS class for the page button. This could be 'page', 'first', 'last', 'next' or 'previous'.
     * @param boolean whether this page button is visible
     * @param boolean whether this page button is selected
     * @return string the generated button
     */
    protected function createPageButton($label, $page, $class, $hidden, $selected)
    {
        if ($hidden || $selected) {
            return CHtml::tag('span', array('class' => 'current'), $label);
        }
        return CHtml::link($label, $this->createPageUrl($page));
    }

    /* @var $dataProvider Leaves */
    /*
        public function renderSummary()
        {
            if(($count=$this->dataProvider->getItemCount())<=0)
                return;

            echo '<div class="'.$this->summaryCssClass.'">';
            if($this->enablePagination)
            {
                $pagination=$this->dataProvider->getPagination();
                $total=$this->dataProvider->getTotalItemCount();
                $start=$pagination->currentPage*$pagination->pageSize+1;
                $end=$start+$count-1;
                if($end>$total)
                {
                    $end=$total;
                    $start=$end-$count+1;
                }
                if(($summaryText=$this->summaryText)===null)
                    $summaryText=Yii::t('zii','Displaying {start}-{end} of 1 result.|Displaying {start}-{end} of {count} results.',$total);
                echo strtr($summaryText,array(
                    '{start}'=>$start,
                    '{end}'=>$end,
                    '{count}'=>$total,
                    '{page}'=>$pagination->currentPage+1,
                    '{pages}'=>$pagination->pageCount,
                ));
            }
            else
            {
                if(($summaryText=$this->summaryText)===null)
                    $summaryText=Yii::t('zii','Total 1 result.|Total {count} results.',$count);
                echo strtr($summaryText,array(
                    '{count}'=>$count,
                    '{start}'=>1,
                    '{end}'=>$count,
                    '{page}'=>1,
                    '{pages}'=>1,
                ));
            }
            echo '</div>';
        }
    */
    /**
     * Override parent function
     * Executes the widget.´
     * This overrides the parent implementation by displaying the generated page buttons.
     */
    public function run()
    {
        $start = $this->currentPage * $this->pageSize + 1;
        $totalItems = $this->getItemCount();
        $end = $start + $this->pageSize - 1;
        $buttons = $this->createPageButtons();
        //$this->renderSummary();
        if (empty($buttons))
            return;
        $this->registerClientScript();
        echo $this->header;
        echo CHtml::tag('div', array('id' => 'pagination'), CHtml::tag('div', $this->htmlOptions, "<span class='pages'>" . "</span>" . implode("\n", $buttons)));
        echo $this->footer;
    }
}

